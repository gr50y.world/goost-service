package service

func mountTaskService(handlers []interface{}) {

	if enabled, ok := cfg["task_enable"].(bool); !ok || !enabled {
		return
	}

	for _, item := range handlers {
		s, ok := item.(TaskService)
		if !ok {
			continue
		}

		s.MountTask()
	}
}
