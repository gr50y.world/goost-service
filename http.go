package service

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	logger "gitlab.com/gr50y.world/goost-logger"
)

type httpService struct {
	Instance *http.Server
}

func (serv *httpService) Shutdown() {
	if serv.Instance == nil {
		return
	}

	if err := serv.Instance.Shutdown(*_ctx); err != nil {
		logger.ErrorF("Shutdown HTTP Service Error, %v", err)
	} else {
		logger.Info("HTTP Service Is Shutdown")
	}

	serv.Instance = nil
}

func mountHTTPService(handlers []interface{}) {

	if enabled, ok := cfg["http_enable"].(bool); !ok || !enabled {
		return
	}

	var r *gin.Engine
	env := os.Getenv("APP_ENV")

	if env == "release" || env == "production" {
		r = gin.New()
	} else {
		r = gin.Default()
	}

	r.Use(func(ctx *gin.Context) {
		ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		ctx.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		ctx.Writer.Header().Set("Access-Control-Allow-Methods", "*")

		if ctx.Request.Method == "OPTIONS" {
			ctx.AbortWithStatus(204)
			return
		}

		ctx.Next()
	})

	r.Use(func(ctx *gin.Context) {
		defer func(ctx *gin.Context, st time.Time) {
			if strings.HasPrefix(ctx.Request.URL.Path, "/assets/") {
				return
			}

			referer := ctx.GetHeader("Referer")
			ctx.Set("Referer", referer)
			ctx.Set("request-durations", fmt.Sprintf("%f", time.Since(st).Seconds()))

			logger.Access(ctx)
		}(ctx, time.Now())

		ctx.Next()
	})

	r.Use(requestid.New())

	host, ok := cfg["http_host"].(string)
	if !ok || host == "" {
		host = "127.0.0.1"
	}

	port, ok := cfg["http_port"].(string)
	if !ok || port == "" {
		port = "80"
	}

	serv := &httpService{
		Instance: &http.Server{
			Addr:         host + ":" + port,
			Handler:      r,
			ReadTimeout:  60 * time.Second,
			WriteTimeout: 60 * time.Second,
			IdleTimeout:  30 * time.Second,
		},
	}

	add(serv)

	for _, item := range handlers {
		s, ok := item.(HttpService)
		if !ok {
			continue
		}

		s.MountHTTP(r)
	}

	go func() {
		if err := serv.Instance.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.ErrorF("HTTP Service Error, %v", err)
			shutdown(serv)
		}
	}()
}
