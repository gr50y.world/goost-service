package service

import (
	"sync"
	"time"

	"github.com/robfig/cron/v3"
	logger "gitlab.com/gr50y.world/goost-logger"
)

type CronJob struct {
	Name     string       `json:"name"`
	Spec     string       `json:"spec"`
	CronFunc func() error `json:"funcName"`
	EntryID  cron.EntryID `json:"entryID"`
	wg       sync.WaitGroup
}

func (c *CronJob) Run() {
	c.wg.Add(1)

	startTime := time.Now()
	err := c.Exec()

	c.wg.Done()

	c.RecordJobStatus(c, startTime, time.Now(), err)
}

func (c *CronJob) Wait() {
	c.wg.Wait()
}

func (c *CronJob) Exec() error {
	return c.CronFunc()
}

func (c *CronJob) SetEntryID(entryID cron.EntryID) {
	c.EntryID = entryID
}

func (c *CronJob) RecordJobStatus(job *CronJob, startTime, endTime time.Time, err error) {
	execTime := endTime.Sub(startTime)
	if err != nil {
		logger.ErrorF("%s, Execute Error , %v , Spent %v", c.Name, err, execTime)
		return
	}
}
