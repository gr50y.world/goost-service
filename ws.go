package service

import (
	logger "gitlab.com/gr50y.world/goost-logger"
)

type wsService struct {
	Instance *WebSocketServer
}

func (serv *wsService) Shutdown() {

}

func mountWebSocketService(handlers []interface{}) {

	if enabled, ok := cfg["ws_enable"].(bool); !ok || !enabled {
		return
	}

	host, ok := cfg["ws_host"].(string)
	if !ok || host == "" {
		host = "127.0.0.1"
	}

	port, ok := cfg["ws_port"].(string)
	if !ok || port == "" {
		port = "8000"
	}

	instance, err := NewWebSocketServer(host, port)
	if err != nil {
		logger.ErrorF("Starting WebSocket Service Error, %v", err)
		return
	}

	serv := &wsService{
		Instance: instance,
	}

	for _, item := range handlers {
		s, ok := item.(WebSocketService)
		if !ok {
			continue
		}

		s.MountWebSocket(serv.Instance)
	}

	go func() {
		if err := serv.Instance.Listen(); err != nil {
			logger.ErrorF("WebSocket Service Error, %v", err)
			shutdown(serv)
		}
	}()
}
