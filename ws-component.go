package service

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	logger "gitlab.com/gr50y.world/goost-logger"
)

var _websocket_routes = []string{}
var _websocket_server WebSocketServer

type WebSocketServer struct {
	Addr     *string
	Upgrader websocket.Upgrader
}

func NewWebSocketServer(host, port string) (*WebSocketServer, error) {

	_websocket_server = WebSocketServer{
		Addr: flag.String("addr", host+":"+port, "http service address"),
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	}

	return &_websocket_server, nil
}

func (ws *WebSocketServer) Register(path string, core func(c *websocket.Conn)) {

	_websocket_routes = append(_websocket_routes, path)

	http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		c, err := ws.Upgrader.Upgrade(w, r, nil)
		if err != nil {
			logger.Error("WebSocket Service Upgrade Error, %v", err)
			return
		}

		defer c.Close()

		core(c)
	})
}

func (ws *WebSocketServer) Listen() error {
	if len(_websocket_routes) > 0 {
		fmt.Printf("\n")

		for _, path := range _websocket_routes {
			fmt.Printf("[ws-path] %s\n", path)
		}

		_websocket_routes = nil
	}

	return http.ListenAndServe(*ws.Addr, nil)
}
