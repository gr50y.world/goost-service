package service

import (
	"github.com/gin-gonic/gin"
)

type Service interface {
	Shutdown()
}

type CronService interface {
	MountCron(jobs *[]*CronJob)
}

type HttpService interface {
	MountHTTP(e *gin.Engine)
}

type WebSocketService interface {
	MountWebSocket(ws *WebSocketServer)
}

type TaskService interface {
	MountTask()
}
