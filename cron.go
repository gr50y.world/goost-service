package service

import (
	"github.com/robfig/cron/v3"
	logger "gitlab.com/gr50y.world/goost-logger"
)

type cronService struct {
	Instance *cron.Cron
	Jobs     []*CronJob
}

func (serv *cronService) Shutdown() {
	for _, job := range serv.Jobs {
		job.Wait()
	}

	logger.Info("CRON Service Is Shutdown")
}

func mountCronService(handlers []interface{}) {

	if enabled, ok := cfg["cron_enable"].(bool); !ok || !enabled {
		return
	}

	jobs := []*CronJob{}

	for _, item := range handlers {
		s, ok := item.(CronService)
		if !ok {
			continue
		}

		s.MountCron(&jobs)
	}

	serv := &cronService{
		Instance: cron.New(cron.WithSeconds()),
		Jobs:     jobs,
	}

	for _, job := range serv.Jobs {
		eid, err := serv.Instance.AddJob(job.Spec, cron.NewChain(cron.SkipIfStillRunning(cron.DefaultLogger), cron.Recover(cron.DefaultLogger)).Then(job))
		if err != nil {
			logger.Error(err)
		}

		job.SetEntryID(eid)
	}

	add(serv)

	serv.Instance.Start()
}
