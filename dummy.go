package service

import (
	"context"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"

	guard "gitlab.com/gr50y.world/goost-guard"
	logger "gitlab.com/gr50y.world/goost-logger"
)

var _chan_all_service_shutdown = make(chan struct{}, 1)
var _ctx *context.Context
var _ServiceCount int32 = 0

var cfg map[string]any
var services []Service

func init() {

	cfg = map[string]any{}

	guard.Deploy()
}

func SetContext(ctx *context.Context) {

	_ctx = ctx
}

func SetConfig(configs map[string]any) {

	cfg = configs
}

func add(s Service) {

	services = append(services, s)

	atomic.AddInt32(&_ServiceCount, 1)
}

func shutdown(s Service) {

	s.Shutdown()

	if atomic.AddInt32(&_ServiceCount, -1) <= 0 {
		close(_chan_all_service_shutdown)
	}
}

func MountServices(handlers []interface{}) {

	mountCronService(handlers)
	mountHTTPService(handlers)
	mountWebSocketService(handlers)
	mountTaskService(handlers)
}

func Listen() {

	quitSignal := make(chan os.Signal)
	signal.Notify(quitSignal, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-_chan_all_service_shutdown:
		logger.Info("⚠️ All Service Is Shutdown ⚠️")
	case <-(*_ctx).Done():
		logger.Info("⚠️ Shutdown With Context Done ⚠️")
	case <-quitSignal:
		logger.Info("⚠️ Shutdown With OS QuitSignal ⚠️")
	}

	for _, s := range services {
		s.Shutdown()
	}
}
